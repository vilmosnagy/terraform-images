ARG BASE_IMAGE

FROM golang:1.14 AS tfplantool

ARG TFPLANTOOL_VERSION
ARG TERRAFORM_BINARY_VERSION

WORKDIR /tfplantool

RUN git clone --branch $TFPLANTOOL_VERSION --depth 1 https://gitlab.com/mattkasa/tfplantool.git .
RUN sed -i -e "/github\.com\/hashicorp\/terraform/s/ v.*\$/ v$TERRAFORM_BINARY_VERSION/" go.mod
RUN go get -d -v ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o tfplantool .

FROM $BASE_IMAGE

ARG TERRAFORM_BINARY_VERSION

RUN dnf -y install jq-1.6-7.fc34  git-2.31.1-3.fc35 openssh-8.5p1-2.fc35 unzip-6.0-52.fc35 && dnf clean all

WORKDIR /tmp

RUN ( curl -sLo terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_BINARY_VERSION}/terraform_${TERRAFORM_BINARY_VERSION}_linux_amd64.zip" && \
      unzip terraform.zip && \
      rm terraform.zip && \
      mv ./terraform /usr/local/bin/terraform \
    ) && terraform --version

WORKDIR /

COPY --from=tfplantool /tfplantool/tfplantool /usr/bin/tfplantool

COPY src/bin/gitlab-terraform.sh /usr/bin/gitlab-terraform
RUN chmod +x /usr/bin/gitlab-terraform

# Override ENTRYPOINT since hashicorp/terraform uses `terraform`
ENTRYPOINT []
